# 项目简介

一份包含面板的扩展，该面板基于 vue2.x 开发，展示了如何通过消息和菜单打开面板，以及与面板通讯。

## 开发环境

Node.js

## 安装

```bash
# 安装依赖模块
npm install
# 构建
npm run build
```
