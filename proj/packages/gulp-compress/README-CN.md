# 项目简介

一份包含面板的扩展，该面板基于 vue3.x 开发，展示了如何通过消息和菜单打开面板，以及与面板通讯。

## 开发环境

Node.js

## 安装

```bash
# 安装依赖模块
npm install
# 构建
npm run build
```

## 用法

如果安装出错，请修改hosts文件
windows : C:\Windows\System32\drivers\etc\hosts
199.232.28.133 raw.githubusercontent.com